#!/usr/bin/env zsh

function check_for_tool {
  local tool=$1
  if ! command -v $tool &> /dev/null
  then
    echo " ‼️  $tool missing"
  else
    if [ "$debug" != "false" ]; then
      local location=$(command -v $tool)
      echo "  $tool found: $location"
    fi
  fi
}

debug=${1:-false}

# check for required file
if [ -r $HOME/.chezmoi-encryption-key.txt ]
then
  if [ "$debug" != "false" ]
  then
    echo "  encryption key for chezmoi"
  fi
else
  echo " ‼️  missing encryption key for chezmoi"
fi

# check for required tools
check_for_tool "age"
check_for_tool "git"
check_for_tool "nvim"
check_for_tool "chezmoi"
check_for_tool "cheat"
check_for_tool "op"
check_for_tool "tmux"
check_for_tool "tig"
check_for_tool "starship"
check_for_tool "jq"
{{- if eq .chezmoi.os "darwin" }}
check_for_tool "brew"
#check_for_tool "nix"
{{- end }}


if ! [ -r $HOME/.zprezto ]; then
  echo "zprezto not installed"
  exit
fi

# only run once every 30 days
if ! [ -r ~/.zprezto-system-checked ] || test `find ~/.zprezto-system-checked -mtime +30`
then
  # record we checked
  touch ~/.zprezto-system-checked
  # check for prezto update lifted from `zprezto-update`
  cd -q -- $HOME/.zprezto || exit 7
  orig_branch="$(git symbolic-ref HEAD 2> /dev/null | cut -d '/' -f 3)"
  if [[ "$orig_branch" == "master" ]]; then
    git fetch || exit "$?"
    UPSTREAM=$(git rev-parse '@{u}')
    LOCAL=$(git rev-parse HEAD)
    REMOTE=$(git rev-parse "$UPSTREAM")
    if [[ "$LOCAL" != "$REMOTE" ]]; then
      echo " ﮮ zprezto-update"
      exit
    fi
  fi
  cd -q -- $HOME
fi

if [[ "$debug" != "false" ]]; then
  if [[ $(git config commit.gpgsign) == "true" ]]; then
    echo "  commit signing is on"
  else
    echo "  commit signing is off"
  fi
  echo "debug enabled"
else
  if [[ $(git config commit.gpgsign) == "true" ]]; then
    echo " "
  else
    echo " "
  fi
fi
