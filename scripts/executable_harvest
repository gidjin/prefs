#!/usr/bin/env zsh

function update_cache {
  if [ -f ~/.hrvst/cache_current.json ]; then
    # How many seconds before file is deemed "older"
    OLDTIME=300
    # Get current and file times
    CURTIME=$(date +%s)
    FILETIME=$(date -r ~/.hrvst/cache_current.json +%s)
    TIMEDIFF=$(expr $CURTIME - $FILETIME)
    if [ $TIMEDIFF -gt $OLDTIME ]; then
      gen_cache_file
    fi
  else
    gen_cache_file
  fi
}

function gen_cache_file {
  today=$(date +"%Y-%m-%d")

  queryConfig='.accountConfig[.accountId].aliases | to_entries | map(. + {alias: .key,aliasId:("p" + (.value.projectId|tostring) + ":t" + (.value.taskId|tostring))}) | INDEX(.[];.aliasId)'

  queryCurrent='map({ id:.id, client:.client.name, project:.project.name, task:.task.name, is_running: .is_running, hours:.hours, aliasId:("p" + (.project.id|tostring) + ":t" + (.task.id|tostring)) }) | map(. + {alias: ($config[0])[.aliasId].alias})'

  jq "$queryConfig" ~/.hrvst/config.json | tee ~/.hrvst/cache_config.json > /dev/null
  hrvst time-entries list --from=$today --output json | tee ~/.hrvst/cache_raw_current.json > /dev/null
  jq "$queryCurrent" --slurpfile config ~/.hrvst/cache_config.json ~/.hrvst/cache_raw_current.json | tee ~/.hrvst/cache_current.json > /dev/null
}

# {
#    log:.[0] | map({project:.project.name,task:.task.name,aliasId:("p" + (.project.id|tostring) + ":t" + (.task.id|tostring))}),
#    config: .[1] | .accountConfig[.accountId].aliases | to_entries | map({alias: .key,aliasId:("p" + (.value.projectId|tostring) + ":t" + (.value.taskId|tostring))}) | INDEX(.[];.aliasId)
#}

function current {
  gen_cache_file
  jq -e 'map(select(.is_running)) | .[0]' ~/.hrvst/cache_current.json
}

function start_timer {
  # does the timer already have an entry?
  #timerObj=$(jq -e 'map(select(.alias == "'$hrvstTimer'" and .is_running == false)) | if (.|length) == 1 then .[0] else false end' ~/.hrvst/cache_current.json)
  timeEntryId=$(jq -e 'map(select(.alias == "'$hrvstTimer'")) | if (.|length) == 1 then .[0].id else false end' ~/.hrvst/cache_current.json)
  if [ $? -eq 0 ]; then
    # resume it
    hrvst time-entries restart --time_entry_id $timeEntryId
  else
    # start it
    hrvst start $hrvstTimer
  fi

  gen_cache_file
}

function stop_timer {
  hrvst stop
  gen_cache_file
}

function list_aliases {
  jq 'map(.alias)' ~/.hrvst/cache_config.json
}

function list_aliases_today {
  jq 'map({ alias: .alias, hours: .hours, is_running: .is_running })' ~/.hrvst/cache_current.json
}

function status_prompt {
  jq --raw-output 'map(. + {"hoursStr": (.hours |tostring | split(".") | map(tonumber) | .[1] = ((.[1]//0)*60/100|round)) | join(":")}) | . as $orig | map(.hours) | add | (.//0)*100 | floor / 100 |tostring | split(".") | map(tonumber) | .[1] = ((.[1]//0)*60/100|round) | join(":") | . as $total| $orig | map(select(.is_running)) | .[0] | if (. | length) == 0 then ("none[0/"+$total+"]") else (.alias+"["+(.hoursStr)+"/"+$total+"]") end' ~/.hrvst/cache_current.json
}

# update the scripts cache if it is time
update_cache

# determine what action was selected and act
while [[ "$#" -gt 0 ]]
do case $1 in
    clear-cache|-f|cache-clear) rm -rvf ~/.hrvst/cache_*
    update_cache &;;
    list|-l|--list-aliases) list_aliases;;
    today|-t|--list-aliases-today) list_aliases_today;;
    current|-c|--current) exec current;; # we exec here to ensure return code from current is returned
    status) status_prompt;;
    stop) stop_timer;;
    start|-s|--start) hrvstTimer="$2"
    if [ -z $hrvstTimer ]; then
      echo "Missing timer argument select one alias:\n"
      list_aliases
      echo "\n$> $0 start <alias>"
      exit 1;
    fi
    start_timer
    shift;;
    crontab) echo "*/15 8-18 * * 1-5 $0 >~/.hrvst/stdout.log 2>~/.hrvst/stderr.log";;
    *) echo "Unknown parameter passed: $1"
    exit 1;;
esac
shift
done
