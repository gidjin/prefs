# ARCHIVED

---

**MOVED TO https://git.sr.ht/~gidjin/prefs**

---

# Personal DOT files

This repo contains my dot files. They are managed by [chezmoi.io](https://www.chezmoi.io/)

## Basics

1. Switch to zsh
2. Install prezto git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
   Have to do this this way not as an external as prezto includes submodules and chezmoi external doesn't get those
   to update `zprezto-update`
3. Install chezmoi and apply config `sh -c "$(curl -fsLS git.io/chezmoi)" -- init --apply gitlab.com/gidjin/prefs`

### Add encrypted config

`chezmoi add --encrypt private_config`

### Install 1password and 1password cli

Install 1password by hand not in app store or brew

Install [1password cli](https://1password.com/downloads/command-line/) and verify the install [see here](https://support.1password.com/command-line-getting-started/)

the cli is used by chezmoi to get keys

## Tool link

### Brew

A lot of my setup relies on [Homebrew](https://brew.sh). Having a Brewfile defined means I can run `brew bundle` to ensure all my required tools are installed and up to date.

#### helpful commands

`brew bundle dump -f` will replace the `Brewfile` in the current directory with what ever the current state is
`brew bundle check` will make sure everything is installed as it should be

### Authy

For auth keys download [Authy](https://authy.com)

Installed by hand

### Fonts

Use the [FiraCode](https://github.com/tonsky/FiraCode/wiki/Installing) fonts for developer fonts
or [NerdFonts](https://github.com/ryanoasis/nerd-fonts#option-4-homebrew-fonts)

My font is intalled via Brew for mac. Also I found the [cheat sheet](https://www.nerdfonts.com/cheat-sheet) helpful for finding icons.

### Rectangle

Window shaping app like spectacle. [Rectangle](https://rectangleapp.com)
Installed by hand download from site

#### Rectangle Pro

upgraded to Rectangle Pro

Installed by hand download from site

### Bartender 4

Manage the menu bar
[Bartender 4](https://www.macbartender.com)

Installed by hand

### Syncthing

For syncing notes, etc [Syncthing download](https://syncthing.net/downloads/)

Installed by hand

### Obsidian

For note taking [Obsidian](https://obsidian.md) synced with syncthing

Installed by hand

### Fantastical

For all calendars in one place [Fantastical](https://flexibits.com/fantastical)

Installed by hand

### Choosy

browser auto selection [Choosy](https://www.choosyosx.com)

Not supported on M1 mac yet

### Boop

Text formatting tool [Boop](https://boop.okat.best)

Installed by AppStore

### AltTab

Make switching apps a bit more like windows/linux. Trying this out. [AltTab](https://alt-tab-macos.netlify.app)

### Krisp

[Krisp.ai](https://ref.krisp.ai/u/u7d17b742b) remove background noise from audio _referral link for longer free trial_

### mute notifications when sharing

[Muzzle](https://muzzleapp.com)

### toggle mute in zoom with global hotkey

[Mute Monster](https://mutemonster.com/)
Also good is [Shush](https://mizage.com/shush/) though it doesn't toggle the status in zoom like MuteMonster

### jq

json cli query tool it's super helpful. [manual](https://stedolan.github.io/jq/manual) [jqplayground for testing](https://jqplay.org/) [homepage](https://stedolan.github.io/jq)

### harvest cli

at work we use harvest. they have a [cli tool](https://kgajera.github.io/hrvst-cli/) that I use

harvest wrapper was helped by [this intro to arguments](https://rowannicholls.github.io/bash/intro/passing_arguments.html)

Use `hrvst login` to sign in and setup oauth token
Use `hrvst alias <aliasName>` to create an alias. The cli tool will prompt for what category

### KnockKnock

[KnockKnock](https://objective-see.org/products/knockknock.html) developed by the [Objective-See](https://objective-see.org/index.html) non profit for mac os security. Searches for persistently installed software, something malware likes to do.

Installed by brew

### Crontab editor

[crontab.guru](https://crontab.guru/#*/15_8-18_*_*_1-5) has a very nice cron editor that will disect the time values into plain english.

### Cheat

[Cheat](https://github.com/cheat/cheat)
[Cheat community cheatsheets](https://github.com/cheat/cheatsheets)
[Alfred plugin](https://github.com/wayneyaoo/alfred-cheat)

### Alacritty

Terminal emulator with yaml configuration [alacritty](https://alacritty.org) [configuration hints](https://github.com/alacritty/alacritty/blob/2bd26fbeb0c51cd8a98ae31d58ea105d1274387a/alacritty.yml#L470)

Installed by brew

### Termdown

my favorite cli timer [termdown](https://github.com/trehn/termdown)
installed via `pip install termdown`

### nodenv

nodenv to install various versions of node. Also use [nodenv-default-packages](https://github.com/nodenv/nodenv-default-packages) to manage global installed tools see config file `dot_nodenv/default-packages`. To update installed tools after file is updated

```sh
nodenv default-packages install 8.8.1   # Reinstall default packages on Node version 8.8.1

nodenv default-packages install --all   # Reinstall default packages on _all_ installed Node versions
```

### Yubikey Git Commit Signing

Using a Yubikey 5 NFC for commit sigining. Setup walkthrough is available [here](https://playbook.truss.dev/docs/infrasec/tutorials/yubikey-configuration). Some of the config files in that doc are in my repo so that chezmoi can make sure they are present. For example changes to my `.zshrc`, `.zshrc_local`, `~/.gnupg/gpg-agent.conf`, `~/Library/LaunchAgents/gpg-agent.plist`, and some required brew tools are in my Brewfile

#### Trouble

Seems sometimes the tty gets messed up so I added the `gpg-connect-agent updatestartuptty /bye` command to the `yubikey-init` function which seems to help. See [this post](https://limbenjamin.com/articles/yubikey-wsl-agent-refused-operation.html) which had more details

### Nix

package management for projects [NixOS](https://nixos.org/download.html#nix-quick-install)
if problems with permissions of profile folder `nix-env --switch-profile /nix/var/nix/profiles/per-user/$USER/profile`. This happend after install since that runs as root

## Trying conventional commits

To aid in commit log history I am experimenting with [Conventional Commits](https://www.conventionalcommits.org/) for this repo going forward
