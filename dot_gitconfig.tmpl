[github]
  user = {{ .github.username }}
[commit]
  gpgsign = false
{{ if eq .location "work" -}}
[user]
  name = John Gedeon
  email = {{ .email_work }}
[includeIf "gitdir:~/code/personal/"]
  path = ~/.gitconfig_personal.inc
[includeIf "gitdir:~/.local/share/chezmoi/"]
  path = ~/.gitconfig_personal.inc
[includeIf "gitdir:~/code/sign/"]
  path = ~/.gitconfig_sign_ssh.inc
{{- else -}}
[user]
  name = John Gedeon
  email = {{ .email_personal }}
{{- end }}
[core]
  excludesfile = ~/.gitignore_global
  autocrlf = false
  eol = LF
  ignorecase = false
; [help]
;   autocorrect = 20
[merge]
  tool = diffconflicts
[mergetool "diffconflicts"]
  cmd = nvim -c DiffConflictsWithHistory \"$MERGED\" \"$BASE\" \"$LOCAL\" \"$REMOTE\"
  trustExitCode = true
[mergetool]
  keepBackup = false
[branch]
  autosetuprebase = always
[alias]
  hist    = log --pretty=format:\"%h %ad | %s%d [%an]\" --graph --date=short
  co      = checkout
  ff      = merge --ff-only
  st      = status
  s       = status
  shift   = stash
  unshift = stash pop
  ci      = commit
  ca      = commit --amend
  rb      = rebase
  branches= branch
  bmv     = branch -m
  shove   = push --force-with-lease
  fezzik  = push --force
  yank    = pull --rebase --autostash
  get     = fetch --all
  me      = log --author=gedeon --committer=gedeon --oneline
  melong  = log --author=gedeon --committer=gedeon
  wip     = "! git add . && git -c commit.gpgsign=false commit -anm \"WIP\""
  unwip   = "! if [ \"$(git log -1 --pretty=%B)\" = \"WIP\" ]; then git reset HEAD^; else echo \"NO WIP!\" && exit 1; fi"
[diff]
  tool = diffconflicts
  renamelimit = 5000
[rerere]
  enabled = false
[push]
  default = simple
  # See https://git-scm.com/docs/git-config#Documentation/git-config.txt-pushautoSetupRemote
  autoSetupRemote = true
[init]
  defaultBranch = main
[filter "lfs"]
  clean = git-lfs clean -- %f
  smudge = git-lfs smudge -- %f
  process = git-lfs filter-process
  required = true
