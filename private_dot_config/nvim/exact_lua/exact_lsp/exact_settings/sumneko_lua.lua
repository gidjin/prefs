return {
  settings = {
    Lua = {
      diagnostics = {
        globals = { 'lsp', 'vim', 'use' },
      },
      workspace = {
        library = {
          [vim.fn.expand '$VIMRUNTIME/lua'] = true,
          [vim.fn.stdpath 'config' .. '/lua'] = true,
        },
      },
    },
  },
}
