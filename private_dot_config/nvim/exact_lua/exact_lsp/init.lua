-- local capabilities = vim.lsp.protocol.make_client_capabilities()
local status_ok, nvim_lsp = pcall(require, "lspconfig")
if not status_ok then
  return
end

ON_ATTACH_FORMAT_ON_SAVE = function(client, bufnr)
  -- format on save
  if client.server_capabilities.documentFormattingProvider then
    vim.api.nvim_create_autocmd("BufWritePre", {
      group = vim.api.nvim_create_augroup("Format", { clear = true }),
      buffer = bufnr,
      callback = function()
        vim.lsp.buf.format({ timeout_ms = 2000 })
      end,
    })
  end
end

require("lsp.lsp-installer")
require("lsp.handlers").setup()
require("lsp.lsp-signature")
require("lsp.null-ls")

-- TypeScript
-- requires language server `npm i -g typescript-language-server`
nvim_lsp.tsserver.setup({
  on_attach = ON_ATTACH_FORMAT_ON_SAVE,
  filetypes = { "typescript", "typescriptreact", "typescript.tsx" },
  cmd = { "typescript-language-server", "--stdio" },
})

-- Dockerfile
-- requires `npm install -g dockerfile-language-server-nodejs`
nvim_lsp.dockerls.setup{}

-- GoLang
-- requires `brew install gopls`
nvim_lsp.gopls.setup{}

-- Emmet - html related
-- npm install -g emmet-ls
nvim_lsp.emmet_ls.setup{}

-- Graphql
-- npm install -g graphql-language-service-cli
nvim_lsp.graphql.setup{}

-- YAML
-- npm install -g yaml-language-server
nvim_lsp.yamlls.setup{}
