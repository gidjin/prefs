local null_ls_status_ok, null_ls = pcall(require, "null-ls")
if not null_ls_status_ok then
  return
end

local formatting = null_ls.builtins.formatting
local diagnostics = null_ls.builtins.diagnostics
local code_actions = null_ls.builtins.code_actions
local hover = null_ls.builtins.hover

null_ls.setup({
  debug = false,
  update_on_insert = true,
  sources = {
    formatting.black.with({ extra_args = { "--fast" } }),
    -- requires stylua install `cargo install stylua`
    formatting.stylua,
    formatting.terraform_fmt,
    formatting.prettierd,
    -- eslint_d requires eslint_d install `npm install -g eslint_d`
    --diagnostics.eslint_d.with({
    --  diagnostics_format = "[eslint] #{m}\n(#{c})",
    --}),
    diagnostics.eslint_d,
    formatting.eslint_d,
    code_actions.eslint_d,
    diagnostics.statix,
    code_actions.gitsigns,
    hover.dictionary,
    diagnostics.jsonlint,
    ---diagnostics.cspell,
    ---code_actions.cspell,
    --     diagnostics.vale.with({
    --       args = function(params)
    --         return {
    --           --'--config=' .. home .. '/.config/vale/vale.ini',
    --           "--no-exit",
    --           "--output",
    --           "JSON",
    --           "--ext",
    --           "." .. vim.fn.fnamemodify(params.bufname, ":e"),
    --         }
    --       end,
    --     }),
  },
  on_attach = ON_ATTACH_FORMAT_ON_SAVE,
})
