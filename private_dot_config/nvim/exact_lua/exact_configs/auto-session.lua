local opts = { noremap = true, silent = true }
local kmap = vim.api.nvim_set_keymap
local status_ok, autosession = pcall(require, 'auto-session')
if not status_ok then
  return
end

autosession.setup({
  log_level = 'info',
  auto_session_enable_last_session = false,
  -- auto_session_root_dir = vim.fn.stdpath('data') .. "/sessions/",
  auto_session_enabled = true,
  auto_save_enabled = true,
  auto_restore_enabled = true,
  -- auto_session_suppress_dirs = nil,
  -- auto_session_use_git_branch = nil,
  -- bypass_session_save_file_types = nil
})

local status_ok_sessionlens, sessionlens = pcall(require, 'session-lens')
if not status_ok_sessionlens then
  return
end

sessionlens.setup({})
kmap("n", "<leader>se", ":lua require('session-lens').search_session()<cr>", opts)
require("telescope").load_extension("session-lens")
