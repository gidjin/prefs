local opts = { noremap = true, silent = true }
local kmap = vim.api.nvim_set_keymap
local status_okay, bufferline = pcall(require, 'bufferline')
if not status_okay then
  return
end

bufferline.setup({
  options = {
    mode = "tabs",
    diagnostics = "nvim_lsp",
    always_show_bufferline = false,
    show_buffer_close_icons = false,
    show_close_icon = false,
    color_icons = true,
    offsets = {
      {
        filetype = "NvimTree",
        text = "🗃  files",
        text_align = "left",
      },
    },
  },
})

-- Key mappings
kmap("n", "gt", ":BufferLineCycleNext<cr>", opts)
kmap("n", "gT", ":BufferLineCyclePrev<cr>", opts)
kmap("n", "gb", ":BufferLinePick<cr>", opts)
-- kmap("n", "<leader>bp", ":BufferLineTogglePin<cr>", opts)
-- kmap("n", "<leader>bc", ":BufferLinePickClose<cr>", opts)
