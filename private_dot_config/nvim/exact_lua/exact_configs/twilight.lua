local opts = { noremap = true, silent = true }
local kmap = vim.api.nvim_set_keymap
local status_ok, twilight = pcall(require, 'twilight')
if not status_ok then
  return
end

kmap("n", "<leader>t", ":Twilight<CR>", opts)

twilight.setup({})
