local opts = { noremap = true, silent = true }
local kmap = vim.api.nvim_set_keymap
local status_ok, telescope = pcall(require, "telescope")
if not status_ok then return end
local actions = require("telescope.actions")
--local icons = require("icons")

telescope.setup({
  defaults = {
    file_ignore_patterns = {
      ".git/*",
      "node%_modules/*",
    },
    mappings = {
      n = {
        ["q"] = actions.close
      },
    },
  },
})

-- faster sorting, requires fzy to be present installed by brew
telescope.load_extension('fzy_native')

-- Key mappings
kmap("n", "<leader>ff", ":lua require('telescope.builtin').find_files({hidden=true})<cr>", opts)
kmap("n", "<leader>fg", ":lua require('telescope.builtin').live_grep()<cr>", opts)
kmap("n", "<leader>fb", ":lua require('telescope.builtin').buffers()<cr>", opts)
kmap("n", "<leader>fh", ":lua require('telescope.builtin').help_tags()<cr>", opts)
