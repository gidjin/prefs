local opts = { noremap = true, silent = true }
local kmap = vim.api.nvim_set_keymap
local status_ok, tree = pcall(require, 'nvim-tree')
if not status_ok then
  return
end

kmap("n", "\\", ":NvimTreeToggle<CR>", opts)
kmap("n", "|", ":NvimTreeFindFileToggle<CR>", opts)
kmap("n", "<leader>r", ":NvimTreeRefresh<CR>", opts)

tree.setup({})
