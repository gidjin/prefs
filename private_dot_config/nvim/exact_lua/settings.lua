local set = vim.opt
local opts = { noremap = true, silent = true }
local kmap = vim.api.nvim_set_keymap

-- set default lang
vim.cmd([[
  language en_US.UTF-8
]])

-- Example config in Lua
vim.g.tokyonight_style = "night"
vim.g.tokyonight_italic_functions = true
vim.g.tokyonight_sidebars = { "qf", "vista_kind", "terminal", "packer" }

-- Change the "hint" color to the "orange" color, and make the "error" color bright red
vim.g.tokyonight_colors = { hint = "orange", error = "#ff0000" }

-- Load the colorscheme
vim.cmd([[colorscheme tokyonight]])

-- comma as the leader for commands
vim.g.mapleader = ","

-- options
-- tab options
set.tabstop = 2
-- Width of autoindent
set.shiftwidth = 2
-- Use shiftwidth to tab at line beginning
set.softtabstop = 2
set.smarttab = true
set.expandtab = true
-- Indentation
set.autoindent = true
set.breakindent = true

-- search options
set.ignorecase = true
set.smartcase = true
set.showmatch = true
-- Press Space to turn off highlighting and clear any message already displayed.
kmap("n", "<space>", ":nohlsearch<Bar>:echo<CR>", opts)

-- Don't fix end of file newline
vim.cmd([[
  set nofixendofline
]])

-- line numbers
set.number = true
-- set.relativenumber = true

-- Show whitespace
set.list = true
-- Trailing white space
set.listchars = "trail:·"

-- Turn on spelling and set language to either english or spanish.
kmap("n", "<leader>ss", ":setlocal spell!<CR>", opts)
-- kmap("n", "<leader>sen", ":setlocal spelllang=en_us<CR>", opts)

-- Copy current file path to system pasteboard
kmap(
  "n",
  "<leader>cp",
  ':let @* = fnamemodify(expand("%"), ":.")<CR>:echo "Copied: ".fnamemodify(expand("%"), ":.")<CR>',
  opts
)
kmap(
  "n",
  "<leader>C",
  ':let @* = fnamemodify(expand("%"), ":.").":".line(".")<CR>:echo "Copied: ".fnamemodify(expand("%"), ":.").":".line(".")<CR>',
  opts
)

-- Gracefully handle holding shift too long after : for common commands
vim.cmd([[
  cabbrev W w
  cabbrev Q q
  cabbrev Qa qa
  cabbrev Wq wq
  cabbrev Wqa wqa
  cabbrev Tabe tabe
  cabbrev Tabc tabc
]])

-- highlighting
kmap("n", "<leader>ol", ":highlight clear OverLength<CR>", opts)
vim.cmd([[
  " Highlight Lines too long
  highlight OverLength ctermbg=red ctermfg=white guibg=#592929
  match OverLength /\%121v.\+/
]])

--" highlight trailing whitespace
--autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
--autocmd BufRead,InsertLeave * match ExtraWhitespace /\s\+$/
--
--" Set up highlight group & retain through colorscheme changes
--highlight ExtraWhitespace ctermbg=red guibg=red
--autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
--map <silent> <leader>ws :highlight clear ExtraWhitespace<CR>

-- Use mouse support in XTerm/iTerm if available

if vim.fn.has("mouse") == 1 then
  set.mouse = "a"
end

-- Write all writeable buffers when changing buffers or losing focus.
-- Save when doing various buffer-switching things.
set.autowriteall = true
vim.cmd([[
  autocmd BufLeave,FocusLost * silent! wall  " Save anytime we leave a buffer or MacVim loses focus.
]])

-- Don't expand tabs in go
vim.cmd([[autocmd BufRead,BufNewFile *.go setlocal noexpandtab]])

-- Auto apply changes in the chezmoi dir
vim.cmd([[autocmd BufWritePost ~/.local/share/chezmoi/* ! chezmoi apply --source-path "%"]])
