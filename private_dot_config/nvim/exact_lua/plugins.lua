-- This file can be loaded by calling `lua require('plugins')` from your init.vim
local fn = vim.fn
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
local packer_bootstrap
if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({ "git", "clone", "--depth", "1", "https://github.com/wbthomason/packer.nvim", install_path })
end

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

-- Automatically run Packer when changing plugins.lua
-- vim.cmd([[
--   augroup packer_user_config
--     autocmd!
--     autocmd BufWritePost plugins.lua source <afile> | PackerCompile
--   augroup end
-- ]])

-- Have packer use a popup window
packer.init({
  display = {
    open_fn = function()
      return require("packer.util").float({ border = "rounded" })
    end,
  },
})

return require("packer").startup(function()
  -- Packer can manage itself
  use("wbthomason/packer.nvim")

  -- An implementation of the Popup API from vim in Neovim
  use("nvim-lua/popup.nvim")

  -- Editorconfig integration written in Lua
  use("gpanders/editorconfig.nvim")

  use({
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
  })

  use({
    "p00f/nvim-ts-rainbow",
    requires = {
      "nvim-treesitter/nvim-treesitter",
    },
  })

  use({
    "andymass/vim-matchup",
    requires = {
      "nvim-treesitter/nvim-treesitter",
    },
  })

  use({
    "nvim-telescope/telescope.nvim",
    requires = {
      "nvim-lua/plenary.nvim",
    },
  })

  use({
    "nvim-telescope/telescope-fzy-native.nvim",
    requires = {
      "nvim-telescope/telescope.nvim",
    },
  })

  use({
    "neovim/nvim-lspconfig",
  })

  use({
    "williamboman/nvim-lsp-installer",
    requires = {
      "neovim/nvim-lspconfig",
    },
  })

  use("folke/tokyonight.nvim")

  use({
    "kyazdani42/nvim-tree.lua",
    requires = {
      "kyazdani42/nvim-web-devicons", -- optional, for file icon
    },
  })

  use({
    "lewis6991/gitsigns.nvim",
    --  tag = 'release', -- To use the latest release
  })

  use({
    "dinhhuy258/git.nvim",
  })

  use({
    "folke/twilight.nvim",
    requires = {
      "nvim-treesitter/nvim-treesitter",
    },
  })

  use("RRethy/vim-illuminate")

  use("rmagatti/auto-session")

  use({
    "rmagatti/session-lens",
    requires = {
      "rmagatti/auto-session",
      "nvim-telescope/telescope.nvim",
    },
  })

  use({
    "hrsh7th/nvim-cmp",
    requires = {
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-emoji",
      "hrsh7th/cmp-nvim-lua",
      "hrsh7th/cmp-cmdline",
      "dcampos/nvim-snippy",
      "dcampos/cmp-snippy",
      "f3fora/cmp-spell",
    },
  })

  use({
    "folke/trouble.nvim",
    requires = "kyazdani42/nvim-web-devicons",
  })

  use({
    "ray-x/go.nvim",
    requires = {
      "mfussenegger/nvim-dap",
      "rcarriga/nvim-dap-ui",
      "theHamsta/nvim-dap-virtual-text",
      "ray-x/guihua.lua",
    },
  })

  use({
    "nvim-lualine/lualine.nvim",
    requires = {
      "SmiteshP/nvim-gps",
      "nvim-treesitter/nvim-treesitter",
    },
  })

  use({
    "akinsho/bufferline.nvim",
    requires = "kyazdani42/nvim-web-devicons",
  })

  use("norcalli/nvim-colorizer.lua")

  -- Markdown preview
  use({
    "iamcco/markdown-preview.nvim",
    run = "cd app && npm install",
    setup = function()
      vim.g.mkdp_filetypes = { "markdown" }
    end,
    ft = { "markdown" },
  })

  use({
    "folke/todo-comments.nvim",
    requires = "nvim-lua/plenary.nvim",
    config = function()
      require("todo-comments").setup({
        -- your configuration comes here
        -- or leave it empty to use the default settings
        -- refer to the configuration section below
      })
    end,
  })

  use("jose-elias-alvarez/null-ls.nvim") -- for formatters and linters

  use("MunifTanjim/prettier.nvim")

  use("tpope/vim-dotenv")
  use({
    "tpope/vim-dadbod",
    requires = "tpope/vim-dotenv",
  })
  use({
    "kristijanhusak/vim-dadbod-ui",
    requires = {
      "tpope/vim-dotenv",
      "tpope/vim-dadbod",
    },
  })
  use({
    "kristijanhusak/vim-dadbod-completion",
    requires = {
      "kristijanhusak/vim-dadbod-ui",
      "tpope/vim-dotenv",
      "tpope/vim-dadbod",
    },
  })

  -- use("lukas-reineke/indent-blankline.nvim")

  -- use("liuchengxu/vista.vim")

  -- use({
  --   "cuducos/yaml.nvim",
  --   requires = {
  --     "nvim-treesitter/nvim-treesitter",
  --     "nvim-telescope/telescope.nvim", -- optional
  --   },
  -- })

  use({
    "windwp/nvim-autopairs",
    requires = {
      "nvim-treesitter/nvim-treesitter",
    },
  })

  use({
    "windwp/nvim-ts-autotag",
    requires = {
      "nvim-treesitter/nvim-treesitter",
    },
  })

  -- Vim non lua plugins below
  -- Advanced diff conflict resolution
  use("whiteinge/diffconflicts")

  -- Number Toggles
  use("jeffkreeftmeijer/vim-numbertoggle")

  -- Better swap control
  use("gioele/vim-autoswap")

  -- Markdown Table of Contents
  use("mzlogin/vim-markdown-toc")

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if packer_bootstrap then
    require("packer").sync()
  end
end)

--
--
-- -- Have packer use a popup window
-- packer.init({
--   display = {
--     open_fn = function()
--       return require("packer.util").float({ border = "rounded" })
--     end,
--   },
-- })
